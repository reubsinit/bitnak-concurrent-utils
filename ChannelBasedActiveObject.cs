﻿namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// A ChannelBaseActiveObject is a utility that can manipulate the state of a single Channel object.
	/// </summary>
	public abstract class ChannelBasedActiveObject<T>: ActiveObject
	{
		// Declaration of private field:
		// <see cref="_channel"/> as a Channel - Used to store the instance of Channel for use and manipulation.
		protected Channel<T> _channel;

		/// <summary>
		/// Initialises a new instance of ChannelBasedActiveObject with a name, as provided by the parameter <paramref name="name"/>. The Channel that 
		/// the new instance of ChannelBasedActiveObject is by default set to a newly instantiated Channel object, unless a Channel is provided in the parameter <paramref name="channel"/>.
		/// </summary>
		/// <param name="name">
		/// Used to specify the name of the ChannelBasedActiveObject.
		/// </param>
		/// <param name="channel">
		/// Used to specify the Channel that the ChannelBasedActiveObject will manipulate.
		/// </param>
		protected ChannelBasedActiveObject (string name, Channel<T> channel = default(Channel<T>)) : base(name)
		{
			_channel = channel;
		}

		/// <summary>
		/// Return the Channel that the ChannelBasedActiveObject is working with.
		/// </summary>
		public Channel<T> Channel
		{
			get
			{
				return _channel;
			}
		}

		/// <summary>
		/// Abstract method. To be implemented by inheriting classes.
		/// </summary>
		/// <param name="data">
		/// Used to specify the data that the ChannelBasedActiveObject will work with.
		/// </param>
		abstract protected void Process(T data);

		/// <summary>
		/// Run the ChannelBasedActiveObject so that it may process data on it's Channel.
		/// </summary>
		protected override void Run()
		{
			//For as long as the ChannelBasedActiveObject is active, process data that is on the Channel.
			while (true)
			{
				Process (_channel.Dequeue ());
			}
		}
	}
}