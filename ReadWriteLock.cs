﻿using System;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Used to control the read/write permissions of threads, where only one thread may be writing at a single point in time and any number of threads may be reading at a single point in time.
	/// </summary>
	public class ReadWriteLock
	{
		// Declaration of private fields:
		// <see cref="_turnStile"/> as a Mutex - Used to ensure that only 1 thread may arrive at an operation at a time.
		// <see cref="_writeTurnStile"/> as a Mutex - Used to ensure that only 1 writer may arrive at a write operation at a time.
		// <see cref="_writePermission"/> as a Mutex - Used to ensure that only 1 writer thread may be operating at a time.
		// <see cref="_readPermission"/> as a Switch - Used to ensure that any number of threads can be reading at a time but none can be writing.
		private Mutex _turnStile = new Mutex();
		private readonly Mutex _writeTurnStile = new Mutex();
		private readonly Mutex _writePermission = new Mutex();
		private Switch _readPermission;

		/// <summary>
		/// Initializes a new instance of ReadWriteLock.
		/// </summary>
		public ReadWriteLock ()
		{
			_readPermission = new Switch(_writePermission);
		}

		/// <summary>
		/// Acquire exclusive reading permission from the ReadWriteLock.
		/// </summary>
		public void AcquireReader()
		{
			//Only allow a single thread through at a time by using a turnstile system.
			_turnStile.Acquire ();
			_turnStile.Release ();
			//Acquire the Switch object, which governs controls over the write permission Semaphore.
			_readPermission.Acquire ();
		}

		/// <summary>
		/// Release reading permissions from the ReadWriteLock.
		/// </summary>
		public void ReleaseReader()
		{
			//Release the Switch object, which governs controls over the write permission Semaphore.
			_readPermission.Release ();
		}

		/// <summary>
		/// Acquire writing permission from the ReadWriteLock.
		/// </summary>
		public void AcquireWriter()
		{
			//Use a seperate turnstile for threads that seek write permission. Writing threads will queue here.
			_writeTurnStile.Acquire ();
			//Enter the standard turnstile, acquire the write permission Mutex and release the standard turnstile to allow potential readers to queue.
			_turnStile.Acquire ();
			_writePermission.Acquire ();
			_turnStile.Release ();
		}

		/// <summary>
		/// Declaration of method, ReleaseWriter:
		/// Release is called on <see cref="_writePermission"/>
		/// followed by calling Release on <see cref="_writeTurnStile"/>.
		public void ReleaseWriter()
		{
			_writePermission.Release ();
			_writeTurnStile.Release ();
		}
	}
}

