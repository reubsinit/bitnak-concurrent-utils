﻿using System;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Controls activity permissions to Threads by implementing a singular token system, where a Thread relying on the Mutex for act permission may only act upon acquiring a the Mutex's single token.
	/// </summary>
	public class Mutex : Semaphore
	{
		// Declaration of private fields:
		// <see cref="_lock"/> as an Object - Exclusive locking object.
		private readonly Object _lock = new Object();

		/// <summary>
		/// Initializes a new instance of Mutex. By default, the Mutex will be instantiated with a single token. Mutex can also be instantiated without an available token.
		/// </summary>
		/// <param name="toBeAcquired">
		/// Used to specify whether or not the Mutex object starts with 1 or 0 tokens. True instantiates a Mutex with no available tokenß.
		/// </param>
		public Mutex (bool toBeAcquired = false) : base(toBeAcquired ? (uint)0 : (uint)1) {}


		/// <summary>
		/// Release a single token into the Mutex.
		/// </summary>
		/// <param name="tokens">
		/// Used to specify the number of tokens the Mutex object will release. Must only ever be 1.
		/// </param>
		public override void Release (uint tokens = 1)
		{
			//If tokens is not 1, throw an exception, as more than one token is being released.
			if (tokens != 1) 
			{
				throw new Exception("Mutex is trying to release too many tokens.");
			}
			//Lock with exclusive _lock object.
			lock (_lock) 
			{
				//If there is alreadya token in the Mutex upon release, throw and exception, as the Mutex already ha it's cap of 1 tokens.
				if (_tokens == 1) 
				{
					throw new Exception("Mutex already contains maximum token count of 1");
				} 
				//Call base.Release.
				base.Release (tokens);
			}
		}
	}
}

