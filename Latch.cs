﻿using System;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Acts as a closed gate where threads may queue. Threads may not continue on with activity past the Latch until the Latch is released.
	/// </summary>
	public class Latch
	{
		// Declaration of private field:
		// <see cref="_mutex"/> as a Mutex - Used to control the state of the Latch.
		private readonly Mutex _mutex;

		/// <summary>
		/// Initializes a new instance of Latch.
		/// </summary>
		public Latch ()
		{
			_mutex = new Mutex (true);
		}

		/// <summary>
		/// Acquire the Latch.
		/// </summary>
		public void Acquire ()
		{
			//Acquire and immediately release the Mutex.
			_mutex.Acquire ();
			_mutex.Release ();
		}

		/// <summary>
		/// Release the Latch.
		/// </summary>
		public void Release ()
		{
			//Release the Mutex.
			_mutex.Release ();
		}
	}
}

