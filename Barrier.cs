﻿using System;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Declaration of public class Barrier:
	/// When instantiated, a Barrier can be used
	/// to ensure that n number of threads (see <see cref="_barrierLimit"/>) are waiting at a virtual
	/// barrier before being allowed to continue / commence with activity.
	/// </summary>
	public class Barrier
	{
		/// <summary>
		/// Declaration of private fields:
		/// <see cref="_barrierLimit"/> as an unsigned int - Used to keep track of the number of threads the barrier can support.
		/// <see cref="_threadCount"/> as an unsigned int - Used to keep track of how many threads have arrived at the barrier.
		/// <see cref="_turnStile"/> as a Mutex - Used to ensure that only 1 thread may arrive at rendezvous at any give point in time.
		/// <see cref="_actPermission"/> as a Semaphore - Used grant actitivy permission to waiting threads.
		/// </summary>
		protected uint _barrierLimit;
		protected uint _threadCount;
		protected Mutex _turnStile;
		protected Semaphore _actPermission;


		/// <summary>
		/// Declaration of Barrier constructor:
		/// Takes <see cref="uint"/><paramref name="barrierLimit"/> as a parameter.
		/// If no value is provided to <paramref name="barrierLimit"/>, the default value is set to 2 (defaulting the object to a Rendezvous in functionality).
		/// <see cref="_threadCount"/> assigned the value of 0.
		/// <see cref="_turnStile"/> instantiated as a new Mutex object with one available token.
		/// <see cref="_actPermission"/> instantiated as a new Semaphore object with no available tokens.
		/// </summary>
		/// <param name="barrierLimit">
		/// Used to specify the number of threads the barrier will support, the value of which is stored in the <see cref="_thread"/> _barrierLimit.
		/// </param>
		public Barrier (uint barrierLimit = 2)
		{
			_barrierLimit = barrierLimit;
			_threadCount = 0;
			_turnStile = new Mutex ();
			_actPermission = new Semaphore ();
		}

		/// <summary>
		/// Declaration of method, Arrive:
		/// A turnstile system is implemented to ensure that only one thread
		/// may arrive at the barrier at a time.
		/// This functionality is governed by the Mutex object in the <see cref="_turnStile"/> field.
		/// As each thread arrives, they wait on the remaining threads to also arrive.
		/// The number of threads waiting at the barrier is kept track of by the <see cref="_threadCount"/> field.
		/// It is only when the maximum number of threads, as stored in <see cref="_barrierLimit"/>, are waiting at the rendezvous that
		/// they are given permission to commence with activity.
		/// Activity permission is governed by the Semaphore object in <see cref="_actPermission"/>
		/// </summary>
		public Boolean Arrive()
		{
			Boolean result = false;

			_turnStile.Acquire ();
			lock (this) 
			{
				_threadCount++;
				if (_threadCount == _barrierLimit) 
				{
					_actPermission.Release (_barrierLimit);
					result = true;
				} 
				else 
				{
					_turnStile.Release ();
				}
			}
			_actPermission.Acquire ();
			lock (this) 
			{
				_threadCount--;
				if (_threadCount == 0) 
				{
					_turnStile.Release ();
				}
			}
			return result;
		}
	}
}

