﻿using System;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Controls access to a single instance of Semaphore. Any number of threads may utilise the Switch, where the first thread to enter gains exclusive access to the Semaphore
	/// and the last thread to leave the Switch releases access to the Semaphore.
	/// </summary>
	public class Switch
	{
		// Declaration of private fields:
		// <see cref="_controlling"/> as a Semaphore - Used to determine access to activity.
		// <see cref="_arrived"/> as an unsigned int - Used to keep track of how many threads have used the Switch.
		// <see cref="_lock"/> as an Object - Exclusive locking object.
		private Semaphore _controlling; 
		private uint _arrived = 0;
		private readonly Object _lock = new Object();

		/// <summary>
		/// Initializes a new instance of Switch with the<see cref="Semaphore"/><paramref name="controlling"/>Semaphore.
		/// </summary>
		/// <param name="controlling">
		/// Specifies the Semaphore object that the Switch will control.
		/// </param>
		public Switch (Semaphore controlling)
		{
			_controlling = controlling;
		}

		/// <summary>
		/// Acquire the Switch.
		/// </summary>
		public void Acquire()
		{
			//Lock the lock object.
			lock (_lock) 
			{
				//Increment the number of threads that have entered the Switch.
				_arrived++;
				//If the first thread has arrived, then acquire the Semaphore that the Switch is controlling.
				if (_arrived == 1) 
				{
					_controlling.Acquire ();
				}
			}
		}

		/// <summary>
		/// Release the Switch.
		/// </summary>
		public void Release()
		{
			//Lock the lock object.
			lock (this) 
			{
				//Decrement the number of threads that are using the Switch.
				_arrived--;
				//If the last thread is about to leave, release the Semaphore that the Switch is controlling.
				if (_arrived == 0) 
				{
					_controlling.Release ();
				}
			}
		}
	}
}

