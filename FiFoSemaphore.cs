﻿using System;
using System.Collections.Generic;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Controls activity permissions to Threads by implementing a token system, where a Thread relying on the Semaphore for act permission may only act upon acquiring a token. First in First out
	/// ensures that the first thread waiting to acquire a token will be the first thread to be able to do so. FiFoSemaphore implements a queue to produce such behaviour.
	/// </summary>
	public class FiFoSemaphore: Semaphore
	{
		// Declaration of private field:
		// <see cref="_queue"/> as a Queue of type Mutex - Used to implement a queueing system for threads waiting on tokens.
		// <see cref="_lock"/> as an Object - Exclusive locking object.
		private Queue<Mutex> _queue = new Queue<Mutex>();
		private readonly Object _lock = new Object();

		/// <summary>
		/// Initializes a new instance of FiFoSemaphore with <see cref="uint"/><paramref name="tokens"/>number of tokens. By default, a FiFoSemaphore is initialised with 1 available tokens.
		/// </summary>
		/// <param name="tokens">
		/// Used to specify the number of tokens the instantiated FiFoSemaphore object will start with.
		/// </param>
		public FiFoSemaphore (uint tokens = 1): base (tokens)
		{
			_tokens = tokens;
		}

		/// <summary>
		/// Acquire a token from the FiFoSemaphore.
		/// </summary>
		public override void Acquire()
		{
			//Declare locak Mutex variable as null.
			Mutex m = null;
			//Lock the _lock object.
			lock (_lock) 
			{
				//Make a check to see if there are no tokens available, if there aren't assign local Mutex variable
				//the value of a new Mutex object with no available tokens and enqueue it to the FiFoSemaphore's queue.
				if (_tokens == 0)
				{
					m = new Mutex (true);
					_queue.Enqueue (m);
				} 
				//Otherwise call base.Acquire.
				else
				{
					base.Acquire ();
				}
			}
			//If the local Mutex object was instantiated, then acquire it
			if (m != null)
			{
				m.Acquire ();
			}
		}

		/// <summary>
		/// By default, release a single token into the FiFoSemaphore. More tokens may be released, where <see cref="uint"/><paramref name="tokens"/>desognates the number of tokens to be released.
		/// </summary>
		/// <param name="tokens">
		/// Used to specify the number of tokens the Semaphore object will release.
		/// </param>
		public override void Release(uint tokens = 1)
		{
			//Lock the _lock object
			lock (this) 
			{
				//Local int declared to keep track of the number of thread Mutex objects to be released.
				uint numToDequeue;
				//Check to see if the number of tokens to release is less than the number of elements in the queue, if so, the number to deuque is assigned the value of tokens.
				if (tokens < _queue.Count) 
				{
					numToDequeue = tokens;
				} 
				//Otherwise, assign the number to dequeue the number of elemnets in the queue.
				else 
				{
					numToDequeue = (uint)_queue.Count;
				}
				//Iterate for the number of times to dequeue and release into each Mutex dequeued from the queue.
				for (int i = 0; i < numToDequeue; i++) 
				{
					_queue.Dequeue ().Release ();
				}
				//If the tokens is greater than the number to dequeue, call base.Release with the difference of the two.
				if (tokens > numToDequeue) 
				{
					base.Release ((tokens - numToDequeue));
				}
			}
		}
	}
}

