﻿using System;
using System.Threading;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// An ActiveObject controls the activity of a single thread.
	/// </summary>
	public abstract class ActiveObject
	{

		// Declaration of private fields:
		// <see cref="_thread"/> as a Thread - Used to store the instance of Thread for use and manipulation.
		private Thread _thread;

		/// <summary>
		/// Initializes a new instance of ActiveObject. The name of the ActiveObject is specified by <paramref name="name">.
		/// </summary>
		/// <param name="name">
		/// Used to specify the name of the thread controlled by the ActiveObject.
		/// </param>
		public ActiveObject(String name)
		{
			_thread = new Thread(Run);
			_thread.Name = name;
		}

		/// <summary>
		/// Starts the activity of the ActiveObject's thread.
		/// </summary>
		public void Start()
		{
			_thread.Start();
		}

		/// <summary>
		/// Abstract method Run. To be implemented by inheriting classes.
		/// </summary>
		protected abstract void Run();
	}
}
