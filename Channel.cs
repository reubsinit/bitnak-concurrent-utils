﻿using System;
using System.Threading;
using BitNak.Collections.Generic;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// A Channel serves as a thread safe message passing pipeline.
	/// </summary>
	public class Channel<T>
	{
		// Declaration of private fields:
		// <see cref="_queue"/> as a Queue of type T - Used to queue messages of type T.
		// <see cref="_access"/> as a Semaphore - Used to determine persmission to the Queue in <see cref="_access"/>.
		// <see cref="_lock"/> as an Object - Exclusive locking object.
		protected Queue<T> _queue = new Queue<T>();
		protected Semaphore _access = new Semaphore(0);
		private readonly Object _lock = new Object(); 

		/// <summary>
		/// Initializes a new Channel.
		/// </summary>
		public Channel ()
		{
			
		}

		/// <summary>
		/// Try to dequeue from the Channel, where the parameter <paramref name="milliseconds"/>denotes the maximum time to wait before conceding a timeout and 
		/// the out parameter <paramref name="result"/>is the value that will be dequeued, should a dequeue be executed successfully.
		/// </summary>
		/// <param name="milliseconds">
		/// Used to specify the number of milliseconds to wait to dequeue.
		/// </param>
		/// <param name="result">
		/// An out parameter where the dequeued value will be returned to.
		/// </param> 
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public virtual bool TryDequeue (int milliseconds, out T result)
		{
			//If milliseconds is less than negative one, assign it the value off negative to avoid dealing with an exception.
			if (milliseconds < -1)
			{
				milliseconds = -1;
			}
			//If a token has successfully been acquired from the Channel's access Semaphore, assign the out variable appropriately and retrun true.
			if (_access.TryAcquire (milliseconds))
			{
				try
				{
					result = _queue.Dequeue ();
					return true;
				}
				catch (ThreadInterruptedException)
				{
					//Catch the exception if it is thrown and force release. Throw!
					_access.ForceRelease();
					throw;
				}
			} 
			//Otherwise, assign the out variable the value of the default type, T, and return false.
			else
			{
				result = default(T);
				return false;
			}
		}

		/// <summary>
		/// Enqueue <paramref name="data"/>to the Channel.
		/// </summary>
		/// <param name="data">
		/// Used to specify what will be enqueued.
		/// </param>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public virtual void Enqueue(T data)
		{
			//Enqueue the data onto the Channel's queue and release a token into the Channel's access Semaphore.
			_queue.Enqueue (data);
			//To avoid being interrupted while releasing, use ForceRelease.
			_access.ForceRelease ();
		}

		/// <summary>
		/// Dequeue from the Channel.
		/// </summary>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public virtual T Dequeue()
		{
			//Calls TryDequeue with an infinite timeout period.
			T result;
			TryDequeue (-1, out result);
			return result;
		}
	}
}

