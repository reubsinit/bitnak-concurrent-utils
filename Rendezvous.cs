﻿using System;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Controls the activity of two threads. A Rendezvous will only let both threads continue with activity once they have both arrived at the Rendezvous. Use a Rendezvous
	/// when once thread may not continue on with activity until the other thread has finished it's designated activities. 
	/// </summary>
	public class Rendezvous: Barrier
	{
		/// <summary>
		/// Initializes a new Rendezvous.
		/// </summary>
		public Rendezvous ()
		{
			_barrierLimit = 2;
			_threadCount = 0;
			_turnStile = new Mutex ();
			_actPermission = new Semaphore ();
		}
	}
}

