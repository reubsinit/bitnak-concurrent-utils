﻿using System;
using System.Threading;
using System.Diagnostics;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// Controls activity permissions to Threads by implementing a token system, where a Thread relying on the Semaphore for act permission may only act upon acquiring a token.
	/// </summary>
	public class Semaphore
	{
		// Declaration of private field:
		// <see cref="_tokens"/> as an unsigned int - Used to keep track of the number of tokens available.
		// <see cref="_threadsWaiting"/> as an unsigned int - Used to keep track of threads waiting to acquire from the Semaphore.
		// <see cref="_lock"/> as an Object - Exclusive locking object.
		protected uint _tokens;
		private uint _threadsWaiting = 0;
		private readonly Object _lock = new Object(); 


		/// <summary>
		/// Initializes a new instance of Semaphore with <see cref="uint"/><paramref name="tokens"/>number of tokens. By default, a Semaphore is instantiated with 0 available tokens.
		/// </summary>
		/// <param name="tokens">
		/// Used to specify the number of tokens the instantiated Semaphore object will start with, stored in the <see cref="_tokens"/> field.
		/// </param>
		public Semaphore (uint tokens = 0)
		{
			//Assign tokens the parameter value.
			_tokens = tokens;
		}

		/// <summary>
		/// Attempt to acquire a token from the Semaphore with no timeout period. The acquiring thread will wait infinitely for a token.
		/// </summary>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public virtual void Acquire ()
		{
			//Calls TryAcuire, passing the parameter value of -1, denoting an infinite timeout period.
			TryAcquire (-1);
		}

		/// <summary>
		/// By default, release a single token into the Semaphore. More tokens may be released, where <see cref="uint"/><paramref name="tokens"/> desognates the number of tokens to be released.
		/// </summary>
		/// <param name="tokens">
		/// Used to specify the number of tokens the Semaphore object will release.
		/// </param>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public virtual void Release (uint tokens = 1)
		{
			//Lock the Semaphore's exclusive locking object _lock. Could throw TIE here.
			lock (_lock)
			{
				//Add the number of newly available tokens to the existing pool.
				_tokens += tokens;
				//If the number of threads waiting on tokens is less than the number of tokens available, pulse all waiting thread.
				if (_threadsWaiting < tokens)
				{
					Monitor.PulseAll (_lock);
				} 
				//Otherwise, only a number of times that is equal to the number of tokens that is available.
				else
				{
					for (int i = 0; i < tokens; i++)
					{
						Monitor.Pulse (_lock);
					}
				}
			}
		}

		/// <summary>
		/// Try to acquire a token from the Semaphore within a specified amount of time, where the parameter <see cref="int"/><paramref name="milliseconds"/> denotes the maximum time to wait before conceding a timeout.
		/// If the thread can acquire, true is returned where as if the thread does not acquire within the specified amount of time (timesout), false is returned.
		/// </summary>
		/// <param name="milliseconds">
		/// Used to specify the number of milliseconds to wait on a token for.
		/// </param>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public bool TryAcquire(int milliseconds)
		{
			//New stopwatch declared in order to help calculate how much longer the thread should wait before timing out.
			Stopwatch watch = new Stopwatch ();
			//int declared in order to keep track of how much longer the thread should wait before timing out.
			int timeLeft;
			//If milliseconds is less than negative one, assign it the value off negative to avoid dealing with an exception.
			if (milliseconds < -1)
			{
				milliseconds = -1;
			}
			//Start the stopwatch.
			watch.Start ();
			//Lock the Semaphore's exclusive locking object _lock.
			lock(_lock)
			{
				//While there are no tokens available, continuously try to acquire a token.
				while (_tokens == 0)
				{
					//The remaining time required to wait is calculated and assigned to timeLeft.
					timeLeft = milliseconds == -1 ? -1 : milliseconds - (int)watch.ElapsedMilliseconds;
					//A thread is waiting for a token, so increment the number of threads waiting.
					_threadsWaiting++;
					try
					{
						//If the timeleft is equal to -1 but the designated wait time is not infinite, then return false
						if (timeLeft < 0 && milliseconds != -1)
						{
							return false;
						}
						//Tell the thread to wait on the lock and the amount of time remaining.
						if (!Monitor.Wait (_lock, timeLeft))
						{
							//If the thread has timed and upon doing so, there is a token available, break the loop and acquire a token. Otherwise return false.
							if (_tokens > 0)
							{
								break;
							}
							return false;
						}
					}
					catch (ThreadInterruptedException)
					{
						//If a thread has been interrupted check to see if any tokens have been released and if so, interrupt the thread.
						if (_tokens > 0)
						{
							Thread.CurrentThread.Interrupt ();
						}
						//Otherwsie, throw the exception.
						else
						{
							throw;
						}
					}
					finally
					{
						//One a thread a either acquire a token or has timed out, decrement the number of threads waiting.
						_threadsWaiting--;
					}
				} 
				_tokens--;
				return true;
			}
		}

		/// <summary>
		/// By default, attempt to force release a single token into the Semaphore. 
		/// More tokens may be forcefully released, where <see cref="uint"/><paramref name="tokens"/> designates the number of tokens to be released.
		/// </summary>
		/// <param name="tokens">
		/// Used to specify the number of tokens the Semaphore object will attempt to forcerelease.
		/// </param>
		public void ForceRelease(uint tokens = 1)
		{
			//TIE variable set to null. Needed incase a thread is interrupted while attempting to force release a token in the Semaphore.
			ThreadInterruptedException ex = null;
			//Infinite post test loop to ensure the designated number of tokens is released.
			while (true)
			{
				try
				{
					//Release the tokens.
					Release(tokens);
					//If the TIE variable references a valid TIE, interrupt the current thread.
					if (ex != null)
					{
						Thread.CurrentThread.Interrupt();
					}
					//Tokens have been released so return.
					return;
				}
				catch (ThreadInterruptedException e)
				{
					//A thread has been interrupted so assign our TIE variable to the thrown TIE.
					ex = e;
				}
			}
		}
	}
}