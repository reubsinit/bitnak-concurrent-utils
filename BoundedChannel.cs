﻿using System.Threading;

namespace BitNak.Concurrent.Utils
{
	/// <summary>
	/// A BoundedChannel serves as a thread safe message passing pipeline with a maximum number of messages that may exist on the BoundedChannel at any given time.
	/// </summary>
	public class BoundedChannel<T>: Channel<T>
	{
		// Declaration of private fields:
		// <see cref="_upperBoundary"/> as a Semaphore - Used to enforce the boundary.
		private readonly Semaphore _upperBoundary;

		/// <summary>
		/// Initializes a new instance of BoundedChannel, where the boundary is denoted by <paramref name="boundary"/>.
		/// </summary>
		/// <param name="boundary">
		/// Used to specify the BoundedChannel's limit.
		/// </param>
		public BoundedChannel (uint boundary)
		{
			//Initialize the boundary Semaphore with <paramref name="boundary"/>number of tokens.
			_upperBoundary = new Semaphore (boundary);
		}

		/// <summary>
		/// Enqueue <paramref name="data"/>to the BoundedChannel.
		/// </summary>
		/// <param name="data">
		/// Used to specify what will be enqueued.
		/// </param>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public override void Enqueue (T data)
		{
			//Acquire from the boundary Semaphore.
			//An exception could be thrown here.
			_upperBoundary.Acquire ();
			//Call base.Enqueue to process the enqueue.
			base.Enqueue (data);
		}

		/// <summary>
		/// Dequeue from the BoundedChannel.
		/// </summary>
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public override T Dequeue ()
		{
			//Assign the value returned from the call to base.Enqueue.
			T data = base.Dequeue ();
			//To avoid being interrupted while releasing, use ForceRelease.
			_upperBoundary.ForceRelease ();
			//Return the dequeued value.
			return data;
		}

		/// <summary>
		/// Try to Enqueue to the BoundedChannel, where <paramref name="data"/>denotes the value to be enqueued to the BoundedChannel 
		/// and <paramref name="milliseconds"/>denotes the maximum time to wait to enqueue before conceding a timeout.
		/// </summary>
		/// <param name="data">
		/// The value to be enqueued.
		/// </param> 
		/// <param name="milliseconds">
		/// Used to specify the number of milliseconds to wait to enqueue.
		/// </param> 
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public bool TryEnqueue (T data, int milliseconds)
		{
			//If milliseconds is less than negative one, assign it the value off negative to avoid dealing with an exception.
			if (milliseconds < -1)
			{
				milliseconds = -1;
			}

			//If a token has successfully been acquired from the BoundedChannel's boundary Semaphore, enqueue the data with base.Enqueue and assign bool true.
			if (_upperBoundary.TryAcquire (milliseconds))
			{
				try
				{
					//An interrupt may occure here.
					base.Enqueue (data);
				}
				catch (ThreadInterruptedException)
				{
					//Catch the interrupt, force release into the boundary and throw the caught exception.
					_upperBoundary.ForceRelease();
					throw;
				}
				return true;
			}
			return false;
		}

		/// <summary>
		/// Try to dequeue from the BoundedChannel, where <paramref name="milliseconds"/>denotes the maximum time to wait before conceding a timeout and 
		/// the out parameter <paramref name="result"/>is the value that will be dequeued, should a dequeue be executed successfully.
		/// </summary>
		/// <param name="milliseconds">
		/// Used to specify the number of milliseconds to wait to dequeue.
		/// </param>
		/// <param name="result">
		/// An out parameter where the dequeued value will be returned to.
		/// </param> 
		/// <exception>May throw <exception cref="System.Threading.ThreadInterruptedException"></exception>
		public override bool TryDequeue (int milliseconds, out T result)
		{
			//If the call to base.TryDequeue succeeds, release a token into the boundary Semaphore ands return true.
			if (base.TryDequeue(milliseconds, out result))
			{
				//Could be interrupted here so use force release then return true.
				_upperBoundary.ForceRelease ();
				return true;
			}
			//Otherwise, return false.
			return false;
		}

	}
}

